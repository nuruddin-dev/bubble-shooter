using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class BubbleScript : MonoBehaviour
{
    public float shootForce;
    public float angle;
    private Rigidbody2D _rigidBody2D;
    private Vector2 _currentDirection;
    public float screenAtMostWidthSize;

    // Start is called before the first frame update
    void Start()
    {
        var directionX = Mathf.Clamp((angle / 90f), -1f, 1f);
        var directionY = 1 - Mathf.Clamp(Mathf.Abs(directionX), 0f, 1f);
        _currentDirection = new Vector2(directionX, directionY);
        _rigidBody2D = gameObject.GetComponent<Rigidbody2D>();
        _rigidBody2D.AddForce(_currentDirection * shootForce);
    }

    private void Update()
    {
        if (gameObject.transform.position.x > screenAtMostWidthSize - transform.localScale.x/2
            || gameObject.transform.position.x < - screenAtMostWidthSize + transform.localScale.x/2)
        {
            var newDirectionX = -_currentDirection.x;
            var newDirectionY = _currentDirection.y;
            var newDirection = new Vector2(newDirectionX, newDirectionY);
            _rigidBody2D.AddForce(newDirection * shootForce);

            StartCoroutine(WaitAndSyncNewDirection(newDirection));
        }
    }

    private IEnumerator WaitAndSyncNewDirection(Vector2 newDirection)
    {
        yield return new WaitForSeconds(.01f);
        _currentDirection = newDirection;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag(StringData.Bubble) && other.GetComponent<SpriteRenderer>().color == transform.GetComponent<SpriteRenderer>().color)
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
