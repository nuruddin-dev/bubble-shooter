using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleMove : MonoBehaviour
{
    public Transform projectileTransform;
    public GameObject shooterPot;
    private float _speed;
    // Start is called before the first frame update
    void Start()
    {
        _speed = 10.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x >= projectileTransform.position.x)
        {
            transform.parent = shooterPot.transform;
            transform.GetComponent<SpriteRenderer>().sortingOrder = 3;
            Destroy(this);
        }
        transform.position = Vector3.Lerp(transform.position, projectileTransform.position, _speed * Time.deltaTime);
    }
}
