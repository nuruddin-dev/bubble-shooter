public abstract class StringData
{
    public const string Player = "Player";
    public const string Bubble = "Bubble";
    public const string NextBubble = "NextBubble";
    public const string CurrentBubble = "CurrentBubble";
}