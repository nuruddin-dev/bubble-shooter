using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public GameObject shooterPot; // GameObject to rotate
    public GameObject bubblePrefab; // Prefab of the projectile object
    public Transform projectileTransform;
    public Transform nextBubbleTransform;
    [SerializeField]
    private float shootForce = 500; // Force applied to the projectile

    private Vector2 _startPos; // Starting position of the touch (Mobile only)
    private bool _isTouchActive; // Is the touch active? (Mobile only)
    private Vector3 _mousePosition; // Current mouse position (PC only)
    private bool _isMouseEntered; // Is the mouse inside the game scene? (PC only)
    private float _angle;
    private Camera _mainCamera;
    private Vector2 _playerBubbleDirection;
    private bool _changeDirectionToLeft;
    private bool _changeDirectionToRight;
    private BubbleScript _bubbleScript;
    private Color[] _colorArray;

    private GameObject _playerBubble;
    private GameObject _currentBubble;
    private GameObject _nextBubble;

    private Color _currentColor;
    private Color _nextColor;
    
    private float _screenAtMostWidthSize;

    private void Start()
    {
        if (Camera.main == null) return;
        _mainCamera = Camera.main;
        _screenAtMostWidthSize = _mainCamera.orthographicSize * _mainCamera.aspect;
        
        // Initialize the array with colors
        _colorArray = new[] { Color.red, Color.green, Color.blue };

        CreateCurrentBubble();
        CreateNextBubble();
        CreateRandomBubble();
    }

    private void CreateRandomBubble()
    {
        for (var i = 0; i < 10; i++)
        {
            var randomPositionX = Random.Range(-_screenAtMostWidthSize, _screenAtMostWidthSize);
            var randomPositionY = Random.Range(5, 10);
            var randomPosition = new Vector2(randomPositionX, randomPositionY);
            var randomColor = GetRandomColor();
            var bubble = Instantiate(bubblePrefab, randomPosition, Quaternion.identity);
            bubble.tag = StringData.Bubble;
            bubble.GetComponent<SpriteRenderer>().color = randomColor;
        }
    }

    private void CreateNextBubble()
    {
        _nextColor = GetRandomColor();
        _nextBubble = Instantiate(bubblePrefab, nextBubbleTransform.position, Quaternion.identity);
        _nextBubble.name = StringData.NextBubble;
        _nextBubble.GetComponent<SpriteRenderer>().color = _nextColor;
    }

    private void CreateCurrentBubble()
    {
        _currentColor = GetRandomColor();
        _currentBubble = Instantiate(bubblePrefab, projectileTransform.position, Quaternion.identity);
        _currentBubble.name = StringData.CurrentBubble;
        _currentBubble.GetComponent<SpriteRenderer>().color = _currentColor;
        _currentBubble.transform.parent = shooterPot.transform;
    }

    void Update()
    {
        // Check if mobile touch or PC mouse is used
        #if UNITY_MOBILE
            // Mobile Touch Interaction
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    startPos = touch.position;
                    isTouchActive = true;
                }
                else if (touch.phase == TouchPhase.Moved && isTouchActive)
                {
                    float deltaX = touch.position.x - startPos.x;

                    float rotation = deltaX * rotationSpeed * Time.deltaTime;
                    objectToRotate.transform.Rotate(0f, 0f, rotation);
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    isTouchActive = false;
                }
            }
        #else
            // PC Mouse Interaction
            _mousePosition = Input.mousePosition;

            if (_isMouseEntered)
            {
                var objectPosition = _mainCamera.ScreenToWorldPoint(_mousePosition);
                objectPosition.z = shooterPot.transform.position.z;

                var direction = objectPosition;
                _angle = direction.x/2 * Mathf.Rad2Deg;
                _angle = Mathf.Clamp(_angle, -90f, 90f);

                shooterPot.transform.rotation = Quaternion.Euler(0f, 0f, -_angle);
            }
        #endif

        // Shoot bubble on click or touch
        if (Input.GetMouseButtonDown(0) || (_isTouchActive && Input.touchCount > 0))
        {
            CreateAndShootPlayerBubble();
            
            CreateCurrentBubbleWithNextColor();

            ChangeNextBubbleColor();

        }
    }

    private void CreateAndShootPlayerBubble()
    {
        _playerBubble = Instantiate(bubblePrefab, shooterPot.transform.position, Quaternion.identity);
        _playerBubble.GetComponent<SpriteRenderer>().color = _currentColor;
        Destroy(_currentBubble);
        _playerBubble.name = StringData.Player;
        _bubbleScript = _playerBubble.AddComponent<BubbleScript>();
        _bubbleScript.shootForce = shootForce;
        _bubbleScript.angle = _angle;
        _bubbleScript.screenAtMostWidthSize = _screenAtMostWidthSize;
    }

    private void CreateCurrentBubbleWithNextColor()
    {
        _currentBubble = Instantiate(bubblePrefab, nextBubbleTransform.transform.position, Quaternion.identity);
        _currentColor = _nextColor;
        _currentBubble.GetComponent<SpriteRenderer>().color = _currentColor;
        var bubbleMove = _currentBubble.AddComponent<BubbleMove>();
        bubbleMove.projectileTransform = projectileTransform;
        bubbleMove.shooterPot = shooterPot;
        _currentBubble.name = StringData.CurrentBubble;
    }

    private void ChangeNextBubbleColor()
    {
        _nextColor = GetRandomColor();
        _nextBubble.GetComponent<SpriteRenderer>().color = _nextColor;
    }


    private Color GetRandomColor()
    {
        if (_colorArray.Length <= 0) return Color.red;
        
        var randomIndex = Random.Range(0, _colorArray.Length);
        return _colorArray[randomIndex];
    }

    void OnMouseEnter()
    {
        _isMouseEntered = true;
    }
}
